export default {
  namespaced: true,

  state: {
    patients: []
  },

  getters: {},

  mutations: {
    add(state, patient) {
      state.patients = [...state.patients, patient];
    }
  },

  actions: {
    register({commit}, patient) {
      commit('add', patient);
    }
  },
};
