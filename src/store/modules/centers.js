export default {
  namespaced: true,

  state: {
    centers: [
      {
        id: "vilnius-a",
        name: "Vilnius A",
        description: "This is Vilnius A description",
        resources: [
          {
            date: "2021-06-15",
            assets: [
              { vaccineId: "pfizer", recipientId: null },
              { vaccineId: "astrazeneca", recipientId: null },
            ],
          },
          {
            date: "2021-06-16",
            assets: [{ vaccineId: "pfizer", recipientId: null }],
          },
          {
            date: "2021-06-19",
            assets: [{ vaccineId: "johnsonandjohnson", recipientId: null }],
          },
        ],
      },
      {
        id: "kaunas-a",
        name: "Kaunas A",
        description: "This is Kaunas A description",
        resources: [
          {
            date: "2021-06-15",
            assets: [
              { vaccineId: "pfizer", recipientId: "some_person_id" },
              { vaccineId: "pfizer", recipientId: null },
              { vaccineId: "astrazeneca", recipientId: null },
            ],
          },
          {
            date: "2021-06-17",
            assets: [{ vaccineId: "astrazeneca", recipientId: null }],
          },
        ],
      },
    ],
  },

  getters: {
    centersCount: (state) => {
      return state.centers.length;
    },

    centerById: (state) => (id) => {
      const [result] = state.centers.filter((center) => center.id == id);
      return result;
    },

    allDatesByCenterId: (state, getters) => (id) => {
      const center = getters.centerById(id);
      return center.resources.map((entry) => entry.date);
    },

    _filterCenterDates: (state, getters) => (centerId, filterCb) => {
      if (!centerId) {
        return null;
      }
      const center = getters.centerById(centerId);

      const filteredResources = center.resources.filter((entry) => {
        const matchingAssets = entry.assets.filter(filterCb);
        return matchingAssets.length > 0 ? entry.date : null;
      });
      return filteredResources.map((entry) => entry.date);
    },

    allCenterAssets: (state, getters) => (centerId) => {
      if (!centerId) {
        return null;
      }
      const center = getters.centerById(centerId);

      let assets = [];
      center.resources.map((resource) => {
        assets = [...assets, ...resource.assets];
      });
      return assets;
    },

    allAvailableDatesByCenterId: (state, getters) => (centerId) => {
      const filterCb = (entry) => entry.recipientId === null;
      return getters._filterCenterDates(centerId, filterCb);
    },

    allDatesByCenterIdVaccineId: (state, getters) => (centerId, vaccineId) => {
      const filterCb = (entry) => entry.vaccineId === vaccineId;
      return getters._filterCenterDates(centerId, filterCb);
    },

    allAvailableDatesByCenterIdVaccineId: (state, getters) => (
      centerId,
      vaccineId
    ) => {
      const filterCb = (entry) =>
        entry.vaccineId === vaccineId && entry.recipientId === null;
      return getters._filterCenterDates(centerId, filterCb);
    },

    allAvailableVaccinesByCenterId: (state, getters) => (centerId) => {
      const assets = getters.allCenterAssets(centerId);
      const distinctVaccines = [];

      if (assets.length > 0) {
        assets.map((asset) => {
          if (!asset.recipientId && !distinctVaccines.includes(asset.vaccineId)) {
            distinctVaccines.push(asset.vaccineId);
          }
        });
      }
      return distinctVaccines;
    },
  },

  mutations: {
    reserveResource(state, requestParams) {
      const { centerId, date, patientId } = requestParams;
      const [center] = state.centers.filter((center) => center.id === centerId);
      const [resource] = center.resources.filter(
        (resource) => resource.date === date
      );
      const freeAsset = resource.assets.filter(
        (asset) => asset.recipientId === null
      );
      // taking the first available
      freeAsset[0].recipientId = patientId;
    },
  },

  actions: {
    reserveResource({ commit }, requestParams) {
      commit("reserveResource", requestParams);
    },
  },
};