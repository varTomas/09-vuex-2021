import Vue from 'vue'
import Vuex from 'vuex'
import vaccines from './modules/vaccines'
import centers from "./modules/centers";
import patients from "./modules/patients";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    vaccines,
    centers,
    patients
  },
});